import time
from typing import Any

import requests
from behave import *
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By


@step('given test_1')
def step_impl(context):
    assert 1 == 1


@step('when_test_1')
def step_impl(context):
    response = requests.get("https://www.google.com/")
    print("response.status_code:", response.status_code)
    assert response.ok


@step('given test_2')
def step_impl(context):
    context.driver.get("https://www.google.com/")


@step('when_test_2')
def step_impl(context):
    driver = context.driver
    title = driver.title
    assert title == "Google", f"{title} not equal Google"


@step('given test_3')
def step_impl(context):
    assert 1 == 1


@step('when_test_3')
def step_impl(context):
    assert 1 == 1

