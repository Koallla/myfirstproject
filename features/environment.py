import datetime
import os
from behave.model_core import Status
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver


def create_browser(context):
    """ Create webdriver"""
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("start-maximized")
    chrome_options.add_argument('--headless')
    # chrome_options.add_argument('--disable-gpu')
    # chrome_options.add_argument('--no-sandbox')
    # chrome_options.add_argument('--window-size=1920,1080')
    # chrome_options.add_argument('--disable-extensions')

    # driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    driver = webdriver.Remote(command_executor='http://selenium__standalone-chrome:4444/wd/hub',
                              desired_capabilities=webdriver.DesiredCapabilities.CHROME)

    return driver


def before_scenario(context, scenario):
    if "browser" in scenario.tags:
        driver = create_browser(context)
        context.driver = driver

    # if not os.path.exists('screenshots'):
    #     os.makedirs('screenshots')

    # delete files in screenshots direction
    # for file_name in os.listdir(context.custom_config['screenshot_dir']):
    #     file_path = os.path.join(context.custom_config['screenshot_dir'], file_name)
    #     if os.path.isfile(file_path):
    #         os.remove(file_path)


def after_scenario(context, scenario):
    if "browser" in scenario.tags:
        context.driver.quit()


# def after_step(context, step):
#     if step.status == Status.failed and hasattr(context, 'driver'):
#         now = datetime.datetime.now()
#         screenshot_path = context.custom_config['screenshot_dir'] + now.strftime("%Y-%m-%d_%H-%M-%S") + '.png'
#         context.driver.save_screenshot(screenshot_path)
