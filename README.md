**MSW API automation test**

Technical requirements

|Package name |Required version|License|
|-------------|--------|---------|
|allure-behave|2.8.29|https://docs.qameta.io/allure/|
|aniso8601|8.1.0|	https://github.com/acapitanelli/aniso-segmentation/blob/master/LICENSE|
|attrs|20.3.0|	https://github.com/python-attrs/attrs/blob/main/LICENSE|
|beautifulsoup4|4.9.3|	https://github.com/wention/BeautifulSoup4/blob/master/COPYING.txt|
|behave|1.2.6|	https://github.com/behave/behave/blob/master/LICENSE|
|cachetools|4.2.0|https://github.com/tkem/cachetools/blob/master/LICENSE|
|certifi|20.12.5|https://github.com/certifi/python-certifi/blob/master/LICENSE|
|cryptography|3.3.1|https://github.com/pyca/cryptography/blob/main/LICENSE|
|et-xmlfile|1.0.1|https://github.com/biydnd/et_xmlfile/blob/master/LICENCE.rst|
|Flask|1.1.2|	https://github.com/pallets/flask/blob/master/LICENSE.rst|
|Flask-RESTful|0.3.8|	https://github.com/flask-restful/flask-restful/blob/master/LICENSE|
|google-api-core|1.25.1|	https://github.com/googleapis/python-api-core/blob/master/LICENSE|
|google-api-python-client|1.12.8|	https://github.com/googleapis/google-api-python-client/blob/master/LICENSE|
|google-auth|1.24.0|https://github.com/googleapis/google-auth-library-python/blob/master/LICENSE|
|google-auth-httplib2|0.0.4|https://github.com/googleapis/google-auth-library-python-httplib2/blob/master/LICENSE|
|googleapis-common-protos|1.52.0|https://github.com/googleapis/api-common-protos/blob/master/LICENSE|
|google-auth-oauthlib|0.4.2|	https://github.com/googleapis/google-auth-library-python-oauthlib/blob/master/LICENSE|
|gspread|3.6.0|	https://github.com/burnash/gspread/blob/master/LICENSE.txt|
|idna|2.10|https://github.com/kjd/idna/blob/master/LICENSE.md|
|jdcal|1.4.1|https://github.com/phn/jdcal/blob/master/LICENSE.txt|
|Jinja2|2.11.2|https://github.com/pallets/jinja/blob/main/LICENSE.rst|
|MarkupSafe|1.1.1|https://github.com/pallets/markupsafe/blob/main/LICENSE.rst|
|marshmallow|3.10.0|https://github.com/marshmallow-code/marshmallow/blob/dev/LICENSE|
|#mysql|0.0.2|	https://github.com/PyMySQL/PyMySQL/blob/master/LICENSE|
|#mysql-connector-python|8.0.23|	https://github.com/mysql/mysql-connector-python/blob/master/LICENSE.txt|
|#mysqlclient|2.0.3|	https://github.com/PyMySQL/mysqlclient/blob/master/LICENSE|
|names|0.3.0|	https://github.com/treyhunner/names/blob/master/LICENSE.txt|
|numpy|1.19.3|	https://github.com/numpy/numpy/blob/main/LICENSE.txt|
|oauth2client|4.1.3|	https://github.com/googleapis/oauth2client/blob/master/LICENSE|
|oauthlib|3.1.0|	https://github.com/oauthlib/oauthlib/blob/master/LICENSE|
|openpyxl|3.0.5|	https://github.com/chronossc/openpyxl/blob/master/LICENCE|
|pandas|1.1.3|	https://github.com/pandas-dev/pandas/blob/master/LICENSE|
|pluggy|0.13.1|https://github.com/pytest-dev/pluggy/blob/master/LICENSE|
|paramiko|2.7.2|	https://github.com/paramiko/paramiko/blob/master/LICENSE|
|psycopg2|2.8.6|	https://github.com/psycopg/psycopg2/blob/master/LICENSE|
|protobuf|3.14.0|https://github.com/protocolbuffers/protobuf/blob/master/LICENSE|
|py7zr|0.11.1|	https://github.com/miurahr/py7zr/blob/master/LICENSE|
|pyotp|2.4.1|	https://github.com/pyauth/pyotp/blob/develop/LICENSE|
|pytz|2020.5|	https://github.com/stub42/pytz/blob/master/LICENSE.txt|
|pyaes|1.6.1|https://github.com/ricmoo/pyaes/blob/master/LICENSE.txt|
|pyasn1|0.4.8|https://github.com/etingof/pyasn1/blob/master/LICENSE.rst|
|pyasn1-modules|0.2.8|https://github.com/etingof/pyasn1-modules/blob/master/LICENSE.txt|
|pycparser|2.20|https://github.com/eliben/pycparser/blob/master/LICENSE|
|pycryptodome|3.9.9|https://github.com/Legrandin/pycryptodome/blob/master/LICENSE.rst|
|PyNaCl|1.4.0|https://github.com/pyca/pynacl/blob/main/LICENSE|
|pyotp|2.4.1|https://github.com/pyauth/pyotp/blob/develop/LICENSE|
|python-dateutil|2.8.1|https://github.com/dateutil/dateutil/blob/master/LICENSE|
|pytz|2020.5|https://github.com/stub42/pytz/blob/master/LICENSE.txt|
|requests-oauthlib|1.3.0|https://github.com/requests/requests-oauthlib/blob/master/LICENSE|
|requests|2.25.1|	https://github.com/psf/requests/blob/master/LICENSE|
|soupsieve|2.1|https://github.com/facelessuser/soupsieve/blob/master/LICENSE.md|
|Telethon|1.19.0|https://github.com/LonamiWebs/Telethon/blob/master/LICENSE|
|texttable|1.6.3|https://github.com/foutaise/texttable/blob/master/LICENSE|
|uritemplate|3.0.1|https://github.com/kylef/URITemplate.swift/blob/master/LICENSE|
|urllib3|1.26.2|https://github.com/urllib3/urllib3/blob/main/LICENSE.txt|
|webargs|7.0.1|https://github.com/marshmallow-code/webargs/blob/dev/LICENSE|
|Werkzeug|1.0.1|https://github.com/pallets/werkzeug/blob/main/LICENSE.rst|
|xlrd|1.2.0|https://github.com/python-excel/xlrd/blob/master/LICENSE|
|zstandard|0.15.1|https://github.com/facebook/zstd/blob/dev/LICENSE|
